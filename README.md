# React Search

This project uses Axios to search [OpenLibrary API](https://openlibrary.org/developers/api).

## Features :star2:

:star: Infinite Scrolling\
:star: Debounce

### Technologies used 🛠️

- [React](https://es.reactjs.org/) - Front-End JavaScript library
- [Axios](https://www.npmjs.com/package/axios) - Promise based HTTP client for node.js

## Try out my code 🚀

This code can be deployed directly on a service such as [Netlify](https://netlify.com). Here is how to install it locally:

### Prerequisites :clipboard:

[Git](https://git-scm.com)\
[NPM](http://npmjs.com)\
[React](https://es.reactjs.org/)

### Setup :wrench:

From the command line, first clone react-search:

```bash
# Clone this repository
$ git clone https://gitlab.com/asuar/react-search.git

# Go into the repository
$ cd react-search

# Remove current origin repository
$ git remote remove origin
```

### Install with NPM

```bash
# Install dependencies
$ npm install

# Start development server
$ npm start
```

Once the server has started, the site can be accessed at `http://localhost:3000/`

## Authors

- **Alain Suarez** - [https://gitlab.com/asuar](https://gitlab.com/asuar)
