import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import './App.css';

const DEBOUNCE_TIME = 1000;
let debounceTimer;

function App() {
  const [query, setQuery] = useState('');
  const [data, setData] = useState([]);
  const [hasMore, setHasMore] = useState(false);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const observer = useRef();

  const lastElement = (node) => {
    if (loading) return;
    if (observer.current) observer.current.disconnect();

    observer.current = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting && hasMore) {
        setPage((prev) => prev + 1);
      }
    });
    if (node) observer.current.observe(node);
  };

  useEffect(() => {
    setData([]);
  }, [query]);

  useEffect(() => {
    setLoading(true);

    const getSearchItems = async () => {
      const response = await axios.get(
        `http://openlibrary.org/search.json?title=${query}&page=${page}`
      );
      setLoading(false);
      setHasMore(response.data.docs.length > 0);
      setData((prev) => {
        return [
          ...new Set([
            ...prev,
            ...response.data.docs.map((item) => item.title),
          ]),
        ];
      });
    };

    getSearchItems();
  }, [query, page]);

  const handleChange = (e) => {
    if (debounceTimer) {
      clearTimeout(debounceTimer);
    }

    debounceTimer = setTimeout(() => {
      setQuery(e.target.value);
      setPage(1);
    }, DEBOUNCE_TIME);
  };

  return (
    <div className="searchContainer">
      <input type="text" onChange={(e) => handleChange(e)} />
      {data.map((item, index) => {
        if (data.length === index + 1) {
          return (
            <div key={index} className="searchTitle" ref={lastElement}>
              {item}
            </div>
          );
        } else {
          return (
            <div key={index} className="searchTitle">
              {item}
            </div>
          );
        }
      })}
      <div>{loading && 'Searching...'}</div>
    </div>
  );
}

export default App;
